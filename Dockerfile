FROM l3iggs/archlinux:latest

MAINTAINER skoppe <bas@21brains.com>

RUN pacman -Sy --noconfirm libevent curl
RUN curl -#sSL -O https://get.docker.com/builds/Linux/x86_64/docker-1.7.1 && chmod +x docker-1.7.1 && mv docker-1.7.1 /usr/local/bin/docker
RUN pacman -S --noconfirm git

ENV WEBHOOK_PORT=33335

VOLUME ["/var/run/docker.sock","/hook"]

WORKDIR /app
ADD phonegap-serve-bb-hook /app/phonegap-serve-bb-hook

CMD ./phonegap-serve-bb-hook