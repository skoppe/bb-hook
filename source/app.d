import vibe.d;
import bbhooks;

void main()
{
	bbhooks.pushes().subscribe((e)
		{
			import std.process;
			import std.stdio;
			auto result = executeShell("./on-push.sh", null, Config.none, size_t.max, "/hook");
			writeln(result.output);
		});
	runEventLoop();
}
