module bbhooks;

import vibe.core.core;
import vibe.core.log;
import vibe.http.router;
import vibe.http.server;
import vibe.web.rest;
import vibe.data.json;

import observe;

alias FiberSubject!Json HookObservable;

private HookObservable observable;

shared static this()
{
	observable = new HookObservable();
	auto router = new URLRouter;
	router.post("/", (scope req, scope res)
	{
		if (auto p = "X-Hook-UUID" in req.headers)
			observable.onNext(req.json);
		res.statusCode = 200;
		res.writeBody("{\"ok\":true}");
	});

	import std.process;
	auto settings = new HTTPServerSettings;
	settings.port = environment.get("WEBHOOK_PORT", "33335").to!ushort;
	settings.bindAddresses = ["0.0.0.0"];

	listenHTTP(settings, router);
}

auto pushes()
{
	return observable.filter!((json){
		return json["push"].to!bool;
	});
}